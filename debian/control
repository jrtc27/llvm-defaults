Source: llvm-defaults
Section: devel
Priority: optional
Maintainer: LLVM Packaging Team <pkg-llvm-team@lists.alioth.debian.org>
Uploaders: Matthias Klose <doko@debian.org>, Sylvestre Ledru <sylvestre@debian.org>
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 9), dpkg-dev (>= 1.13.9), lsb-release,
 m4
Vcs-Svn: svn://svn.debian.org/svn/pkg-llvm/llvm-defaults/trunk/
Vcs-Browser: http://svn.debian.org/viewsvn/pkg-llvm/llvm-defaults/trunk/

Package: llvm
Architecture: any
Depends: llvm-runtime (= ${binary:Version}), llvm-${pv:llvm} ${reqv:llvm}, ${misc:Depends}
Description: Low-Level Virtual Machine (LLVM)
 The Low-Level Virtual Machine (LLVM) is a collection of libraries and
 tools that make it easy to build compilers, optimizers, Just-In-Time
 code generators, and many other compiler-related programs.
 .
 This is a dependency package providing the default llvm package.

Package: llvm-runtime
Architecture: any
Depends: llvm-${pv:llvm}-runtime ${reqv:llvm}, ${misc:Depends}
Description: Low-Level Virtual Machine (LLVM), bytecode interpreter
 The Low-Level Virtual Machine (LLVM) is a collection of libraries and
 tools that make it easy to build compilers, optimizers, Just-In-Time
 code generators, and many other compiler-related programs.
 .
 This is a dependency package providing the default bytecode interpreter.

Package: llvm-dev
Architecture: any
Depends: llvm-runtime (= ${binary:Version}), llvm-${pv:llvm}-dev ${reqv:llvm}, ${misc:Depends}, llvm
Description: Low-Level Virtual Machine (LLVM), libraries and headers
 The Low-Level Virtual Machine (LLVM) is a collection of libraries and
 tools that make it easy to build compilers, optimizers, Just-In-Time
 code generators, and many other compiler-related programs.
 .
 This is a dependency package providing the default libraries and headers.

# Package: libllvm-ocaml-dev
# Architecture: amd64 arm64 armel armhf i386
# Section: ocaml
# Depends: llvm-runtime (= ${binary:Version}), libllvm-${pv:llvm}-ocaml-dev ${reqv:llvm}, ${misc:Depends}
# Description: Low-Level Virtual Machine (LLVM), bindings for OCaml
#  The Low-Level Virtual Machine (LLVM) is a collection of libraries and
#  tools that make it easy to build compilers, optimizers, Just-In-Time
#  code generators, and many other compiler-related programs.
#  .
#  This is a dependency package providing the default bindings for OCaml.

Package: clang
Architecture: any
Depends: clang-${pv:llvm} ${reqv:llvm}, ${misc:Depends}
Replaces: clang (<< 3.2-1~exp2), clang-3.2, clang-3.3, clang-3.4 (<< 1:3.4.2-7~exp1),
 clang-3.5 (<< 1:3.5~+rc1-3~exp1)
Breaks: clang-3.2, clang-3.3, clang-3.4 (<< 1:3.4.2-7~exp1), clang-3.5 (<< 1:3.5~+rc1-3~exp1)
Description: C, C++ and Objective-C compiler (LLVM based)
 Clang project is a C, C++, Objective C and Objective C++ front-end
 for the LLVM compiler. Its goal is to offer a replacement to the GNU Compiler
 Collection (GCC).
 .
 Clang implements all of the ISO C++ 1998, 11 and 14 standards and also
 provides most of the support of C++17.
 .
 This is a dependency package providing the default clang compiler.

Package: clang-tools
Architecture: any
Depends: clang ${reqv:llvm}, ${misc:Depends}
Description: clang-based tools
 Clang project is a C, C++, Objective C and Objective C++ front-end
 for the LLVM compiler. Its goal is to offer a replacement to the GNU Compiler
 Collection (GCC).
 .
 Clang implements all of the ISO C++ 1998, 11 and 14 standards and also
 provides most of the support of C++17.
 .
 This is a dependency package providing the clang tools package.

Package: clang-tidy
Architecture: any
Depends: clang-tidy-${pv:llvm} ${reqv:llvm}, ${misc:Depends}
Breaks: clang (<< 1:3.6-28)
Replaces: clang (<< 1:3.6-28)
Description: clang-based C++ linter tool
 Provide an extensible framework for diagnosing and fixing typical programming
 errors, like style violations, interface misuse, or bugs that can be deduced
 via static analysis. clang-tidy is modular and provides a convenient interface
 for writing new checks.
 .
 clang-tidy replaces clang-modernize
 .
 This is a dependency package providing the clang tidy tool.

Package: clang-format
Architecture: any
Depends: clang-format-${pv:llvm} ${reqv:llvm}, ${misc:Depends}
Breaks: clang (<< 1:3.6-28)
Replaces: clang (<< 1:3.6-28)
Description: Tool to format C/C++/Obj-C code
 Clang-format is both a library and a stand-alone tool with the goal of
 automatically reformatting C++ sources files according to configurable
 style guides. To do so, clang-format uses Clang's Lexer to transform an
 input file into a token stream and then changes all the whitespace around
 those tokens. The goal is for clang-format to both serve both as a user
 tool (ideally with powerful IDE integrations) and part of other
 refactoring tools, e.g. to do a reformatting of all the lines changed
 during a renaming.
 .
 This is a dependency package providing the clang format tool.

Package: libclang1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${dep:devlibs}, libclang1-${pv:llvm} ${reqv:llvm}
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: C, C++ and Objective-C compiler (LLVM based)
 Clang project is a C, C++, Objective C and Objective C++ front-end
 for the LLVM compiler. Its goal is to offer a replacement to the GNU Compiler
 Collection (GCC).
 .
 Clang implements all of the ISO C++ 1998, 11 and 14 standards and also
 provides most of the support of C++17.
 .
 This is a dependency package providing the default clang libraries.
 .
 The C Interface to Clang provides a relatively small API that exposes
 facilities for parsing source code into an abstract syntax tree (AST),
 loading already-parsed ASTs, traversing the AST, associating physical source
 locations with elements within the AST, and other facilities that support
 Clang-based development tools.

Package: libclang-dev
Architecture: any
Section: libdevel
Depends: ${shlibs:Depends}, ${misc:Depends}, ${dep:devlibs},
 libclang-${pv:llvm}-dev ${reqv:llvm}
Description: clang library - Development package
 Clang project is a C, C++, Objective C and Objective C++ front-end
 for the LLVM compiler. Its goal is to offer a replacement to the GNU Compiler
 Collection (GCC).
 .
 Clang implements all of the ISO C++ 1998, 11 and 14 standards and also
 provides most of the support of C++17.
 .
 This is a dependency package providing the default libclang libraries and
 headers.

Package: lldb
Architecture: any
Depends: lldb-${pv:llvm} ${reqv:llvm}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Next generation, high-performance debugger
 LLDB is a next generation, high-performance debugger. It is built as a set of
 reusable components which highly leverage existing libraries in the larger LLVM
 Project, such as the Clang expression parser and LLVM disassembler.
 .
 This is a dependency package providing the default version of lldb.

Package: lld
Architecture: amd64 armel armhf i386  kfreebsd-amd64 kfreebsd-i386 s390 sparc alpha hppa m68k powerpcspe ppc64 sh4 sparc64 x32 mips mipsel
Depends: lld-${pv:llvm} ${reqv:llvm}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: LLVM-based linker
 LLD is a new, high-performance linker. It is built as a set of reusable
 components which highly leverage existing libraries in the larger LLVM
 Project.
